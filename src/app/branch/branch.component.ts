import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: '[app-branch]',
  templateUrl: './branch.component.html',
  styleUrls: ['./branch.component.css']
})
export class BranchComponent implements OnInit {

  @Input() branchName:string
  constructor() { }

  ngOnInit(): void {
  }

}
