import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { TotalComponent } from './total/total.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BranchComponent } from './branch/branch.component';
import { WeekRowComponent } from './week-row/week-row.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    TotalComponent,
    BranchComponent,
    WeekRowComponent,
  ],
  imports: [
    BrowserModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
