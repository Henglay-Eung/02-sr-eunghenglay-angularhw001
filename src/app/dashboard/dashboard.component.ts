import { Week } from './../interfaces/week';
import { Status } from './../interfaces/status';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  week:Week

  cards:Status[]

  generateCard(){
    return this.cards=[
      {used:this.generateNumber(30),available:30},
      {used:this.generateNumber(20),available:20},
      {used:this.generateNumber(25),available:25}
    ]
  }

  generateWeek(){
    return this.week={
      name:"week ",
      regular:{used:this.generateNumber(30),available:30},
      diesel:{used:this.generateNumber(20),available:20},
      premium:{used:this.generateNumber(25),available:25}
    }
  }

  generateNumber(max:number){
    return Math.floor(Math.random() * Math.floor(max))
  }

  constructor() { }

  ngOnInit(): void {
    this.generateCard()
  }

}
