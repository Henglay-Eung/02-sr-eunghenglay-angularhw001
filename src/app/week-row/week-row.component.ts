import { Week } from './../interfaces/week';
import { AfterViewInit, Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';

@Component({
  selector: '[app-week-row]',
  templateUrl: './week-row.component.html',
  styleUrls: ['./week-row.component.css']
})
export class WeekRowComponent implements OnInit,OnChanges {

  @Input() data:Week
  @Input() weekNo:number
  percentageOfRegular:number
  percentageOfDiesel:number
  percentageOfPremium:number
  constructor() { }
  ngOnChanges(changes: SimpleChanges): void {
    this.percentageOfRegular=this.data.regular.used/this.data.regular.available
    this.percentageOfDiesel=this.data.diesel.used/this.data.diesel.available
    this.percentageOfPremium=this.data.premium.used/this.data.premium.available
    console.log(this.data.regular.used)
  }
 
  ngOnInit(): void {
    
  }

}
