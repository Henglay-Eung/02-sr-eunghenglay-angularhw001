import { Status } from './../interfaces/status';
import { AfterContentInit, Component, DoCheck, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-total',
  templateUrl: './total.component.html',
  styleUrls: ['./total.component.css']
})
export class TotalComponent implements OnInit{

  @Input() cardData:Status[]
  cardPercentageRegular:number
  cardPercentageDiesel:number
  cardPercentagePremium:number
  constructor() { }

  ngOnInit(): void {
    this.cardPercentageRegular=this.cardData[0].used/this.cardData[0].available
    this.cardPercentageDiesel=this.cardData[1].used/this.cardData[1].available
    this.cardPercentagePremium=this.cardData[2].used/this.cardData[2].available
  }
  
}
